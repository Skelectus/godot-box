extends VehicleBody

puppet var puppet_transform

var seat0 # Driver's seat. It is required.

var hud
var speedo

puppet var steer = 0
var has_driver = false

const MAX_STEERING_ANGLE = 33
const MAX_ENGINE_FORCE = 200
const STEERING_SPEED = 0.1


func _ready():
	set_steering(0)
	set_engine_force(0)
	set_brake(20)
	
	hud = get_node("hud")
	speedo = hud.get_node("box/speedometer")
	hud.get_node("box/vehicle_name").text = name
	hud.hide()
	
	if get_tree().get_network_peer():
		puppet_transform = transform
	
	# Get driver's seat.
	seat0 = get_node("seat0")
	if seat0:
		seat0.is_driver = true
	else:
		push_error(str(get_path()) + ":   seat0's seat not found. Add a vehicle seat named \"seat0\"")


func _process(_delta):
	speedo.text = str(int(linear_velocity.length() * 3.6)) + " km/h"
	#speedo.text = str(int(linear_velocity.length() * 2.237)) + " mph"


func _physics_process(_delta):
	# Multiplayer transform sync
	if get_tree().get_network_peer():
		if is_network_master():
			rset("puppet_transform", transform)
			rset("steer", steer)
		else:
			transform = puppet_transform
	
	# Vehicle control
	if seat0:
		# Is player
		if!get_tree().get_network_peer() or is_network_master():
			if seat0.player_node:
				var control = Vector3()
				
				# Throttle
				if Input.is_action_pressed("vehicle_throttle"):
					control.y += 1
				if Input.is_action_pressed("vehicle_reverse"):
					control.y -= 1
				control.y *= MAX_ENGINE_FORCE
				
				
				# Steering
				if Input.is_action_pressed("vehicle_steer_left"):
					control.x += 1
				if Input.is_action_pressed("vehicle_steer_right"):
					control.x -= 1
				# Interpolate steering because keyboard input is only on/off
				var target_steer = control.x * deg2rad(MAX_STEERING_ANGLE)
				steer = steer * (1 - STEERING_SPEED) + target_steer * STEERING_SPEED
				
				
				# Brakes
				var temp_brake = 0
				
				# Small amount of braking simulates drag when player is off the gas
				if control.y == 0:
					temp_brake = .5
				
				# Player presses brakes
				if Input.is_action_pressed("vehicle_brake"):
					control.z += 1
					temp_brake = control.z * 30
				
				# Vehicle is almost stopped, stop it automatically
				elif control.y == 0 and linear_velocity.length() * 3.6 < 1:
					temp_brake = 5
				
				
				set_brake(temp_brake)
				set_steering(steer)
				set_engine_force(control.y)
			else:
				set_brake(30)
				set_engine_force(0)
		
		# Is puppet
		else:
			set_steering(steer)
