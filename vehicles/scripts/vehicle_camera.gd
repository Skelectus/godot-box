extends Spatial

var mouse_sensitivity = 0.2
var camera_angle = 0
var camera_change = Vector2()


func _ready():
	pass


func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		if get_parent().player_node:
			aim()


func _input(event):
	if event is InputEventMouseMotion:
		camera_change = event.relative


func aim():
	if camera_change.length() > 0:
		rotate_y(deg2rad(-camera_change.x * mouse_sensitivity))
		var change = camera_change.y * mouse_sensitivity
		if camera_angle + change < 70 and camera_angle + change > -15:
			$pivot_x.rotate_x(deg2rad(change))
			camera_angle += change
		camera_change = Vector2 (0,0)
