# vehicle_seat.gd
# Used for enterable "seats"
# Do NOT attach this to the vehicle's root node!
# Create a child Spatial Node instad.
# For example .../my_vehicle_body/my_spatial_node

extends Spatial

var player_node
var master_id # see _process() --> multiplayer 
var is_driver = false

var exit_node
var hud
var camera
#var occupied_indicator # For debug, to be removed


func _ready():
	if get_parent().get("hud"):
		hud = get_parent().hud
	elif get_parent().get_node("hud"):
		hud = get_parent().get_node("hud")
	
	camera = get_node("vehicle_camera/pivot_x/Camera")
	exit_node = get_node("exit_node")
	#occupied_indicator = get_node("occupied_indicator")


func _process(_delta):
	# Singleplayer
	if !get_tree().get_network_peer():
		if player_node:
			if Input.is_action_just_pressed("Interact"):
				player_node.get_node("Head/Camera/HUD").show()
				if hud:
					hud.hide()
				exit_vehicle()
	
	# Multiplayer
	else:
		if player_node:
			# Entering a car's driver seat hijacks all passenger seats too. This is a duct tape tier solution for passenger seats.
			if get_network_master() != master_id:
				set_network_master(master_id)
			
			# Player
			if is_network_master():
				if Input.is_action_just_pressed("Interact"):
					player_node.get_node("Head/Camera/HUD").show()
					if hud:
						hud.hide()
					rpc("_exit_vehicle")
		
		# Why in hell does this not work?!
		if is_queued_for_deletion():
			rpc("_remove")
			# Let player out before being deleted
			if player_node:
				$"/root/Main/".add_child(player_node)
				player_node.get_node("Head/Camera").make_current()


# If multiplayer, rpc call this instead
sync func _enter_vehicle(_player_node):
	enter_vehicle(_player_node)


func enter_vehicle(_player_node):
	if player_node:
		push_warning(str(get_path()) + " enter_vehicle() was called while already occupied!")
		exit_vehicle()
	player_node = get_node(_player_node)
	
	# Switch to vehicle camera
	if !get_tree().get_network_peer() or player_node.is_network_master():
		camera.make_current()
		player_node.get_node("Head/Camera/HUD").hide()
		if hud:
			hud.show()
	
	# Give vehicle ownership to player
	if get_tree().get_network_peer():
		master_id = player_node.get_network_master()
		if is_driver:
			get_parent().set_network_master(master_id)
		else:
			set_network_master(master_id)
	
	player_node.get_parent().remove_child(player_node)
	#occupied_indicator.show()
	


# If multiplayer, rpc call this instead
sync func _exit_vehicle():
	exit_vehicle()


func exit_vehicle():
	if player_node:
		$"/root/Main/".add_child(player_node)
		player_node.transform.origin = exit_node.global_transform.origin
		
		# Switch back to player's internal camera
		if !get_tree().get_network_peer() or player_node.is_network_master():
			player_node.get_node("Head/Camera").make_current()
		player_node = null
	else:
		push_warning(str(get_path()) + " exit_vehicle() was called, but it is unoccupied!")
	
	# Give the vehicle back to the server
	if get_tree().get_network_peer():
		master_id = null
		if is_driver:
			get_parent().set_network_master(1)
		else:
			set_network_master(1)
	
	#occupied_indicator.hide()


remote func _remove():
	queue_free()
