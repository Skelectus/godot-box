# Main.gd
# Handles gameplay stuff
# Path of the attached node has to be /root/Main
#
# Requires following child spatial nodes:
# - player_spawn_point 
# - object_container (interactive props and such should be spawned in here. Make sure it's translation is ZERO IN ALL AXES) 
#
extends Spatial

var objects = {}



func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	var new_player = preload('res://player/Player.tscn').instance()
	
	# Multiplayer setup
	if get_tree().get_network_peer():
		var my_id = get_tree().get_network_unique_id()
		
		# Player
		new_player.name = str(my_id)
		new_player.set_network_master(my_id)
		if my_id != 1:
			Network.rpc_id(1, "_request_players", my_id)
		
		var _objects = $object_container.get_children()
		
		# If this game is the host, add object_container children to objects{}
		if my_id == 1:
			for child in _objects:
				# adds an unique number to name: 123_objectnamehere
				child.name = str(objects.size()) + "_" + (child.name)
				objects[child.name] = child.filename
		# if this game is a client, just delete everything. We're going to ask the server what to put there.
		else:
			for child in _objects:
				child.queue_free()
			rpc_id(1, "_request_objects", my_id)
	
	# Drop player to game
	add_child(new_player)
	if get_node("player_spawn_point"):
		new_player.transform.origin = $player_spawn_point.transform.origin
	
	



func _process(_delta):
	if(Input.is_action_just_pressed("ui_cancel")):
		_quit_to_main_menu()


func _quit_to_main_menu():
		get_tree().paused = true
		# Check for and disconnect network
		if get_tree().get_network_peer():
			get_tree().set_network_peer(null)
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://mainmenu/main_menu.tscn")

# Use this on sp or host game
func instance_object(_res):
	var prop = load(_res).instance()
	# changes name to something like this: 123_barrel
	prop.name = str(objects.size()) + "_" + (_res.get_file().get_basename())
	# Add it's info to objects dictionary
	objects[prop.name] = _res
	$object_container.add_child(prop)
	return prop

# Use this on client game
remote func instance_existing_object(_res, _name):
	var prop = load(_res).instance()
	prop.name = _name
	# Add it's info to objects dictionary
	objects[prop.name] = _res
	$object_container.add_child(prop)
	return prop


sync func delete_object(path):
	get_node(path).queue_free()


remote func _request_objects(id):
	rpc_id(id, "_set_objects", objects.duplicate())

remote func _set_objects(_objects):
	for _name in _objects:
		instance_existing_object(_objects[_name], _name)
	get_tree().paused = false
