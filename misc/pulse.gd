tool
extends RichTextEffect
class_name Pulse

var bbcode = "pulse"

func _process_custom_fx(char_fx):
	var freq = char_fx.env.get("freq", 2.0)
	
	char_fx.color.a = (sin(char_fx.elapsed_time * freq) + 1.0) / 2.0
	
	return true
