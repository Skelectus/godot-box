extends KinematicBody

#	This is more or less based on Jeremy Bullock's tutorial
#	https://www.youtube.com/playlist?list=PLTZoMpB5Z4aD-rCpluXsQjkGYgUGUZNIV

var mouse_sensitivity = 0.5
var camera_angle = 0
var camera_change = Vector2()

var velocity = Vector3()
var direction = Vector3()

var gravity = 9.8 * 3
var has_contact = false
var override = false
var noclip = false

puppet var puppet_translation = Vector3()
puppet var body_basis = Basis()
puppet var head_basis = Basis()

const WALK_SPEED = 6
const WALK_SPEED_SPRINT = 12
const WALK_SPEED_CROUCH = 2
const WALK_ACCEL = 6
const WALK_DECEL = 5
const JUMP_VEL = 10
const MAX_SLOPE_ANGLE = 35
const MAX_STEP_ANGLE = 15	# stairs, etc. max step normal angle. Used to prevent stepping up in slopes.
const STEP_JUMP_VEL = 20	# This value is suspiciously high as it will be multiplied by step height ( mostly around 0.2 to 0.5 times)

const FLY_SPEED = 15
const FLY_SPEED_SPRINT = 30
const FLY_SPEED_CROUCH = 2
const FLY_ACCEL = 10
const FLY_DECEL = 10

func _ready():
	pass # Replace with function body.


func _process(_delta):
	if Input.is_action_just_pressed("toggle_noclip"):
		noclip = !noclip
		$CollisionShape.disabled = !$CollisionShape.disabled


func _physics_process(delta):
	# This instance is the player (sp or mp)
	if !get_tree().get_network_peer() or is_network_master():
		if override:
			pass
		else:
			aim()
			if noclip:
				move_noclip(delta)
			else:
				move_fps(delta)
		# multiplayer movement sync
		if get_tree().get_network_peer():
			rset_unreliable('puppet_translation', translation)
			rset_unreliable('body_basis', get_node('Head').transform.basis)
			rset_unreliable('head_basis', get_node("Head/Camera").transform.basis)
	
	# This instance is a multiplayer player puppet
	else:
		translation = puppet_translation
		get_node('Head').transform.basis = body_basis
		get_node('Head/head').transform.basis = head_basis


func move_fps(delta):
	#reset player dir
	direction = Vector3()
	
	#get cam rotation
	var aim = $Head.get_global_transform().basis
	
	#inputs
	if Input.is_action_pressed("move_forward"):
		direction -= aim.z
	if Input.is_action_pressed("move_backward"):
		direction += aim.z
	if Input.is_action_pressed("move_left"):
		direction -= aim.x
	if Input.is_action_pressed("move_right"):
		direction += aim.x
		direction.y = 0
	direction = direction.normalized()
	
	var target = direction * WALK_SPEED
	if Input.is_action_pressed("move_slow"):
		target = direction * WALK_SPEED_CROUCH
	elif Input.is_action_pressed("move_sprint"):
		target = direction * WALK_SPEED_SPRINT
	
	#jump
	if is_on_floor():
		has_contact = true
		var n = $GroundRay.get_collision_normal()
		var floor_angle = rad2deg(acos(n.dot(Vector3(0,1,0))))
		if floor_angle > MAX_SLOPE_ANGLE:
			velocity.y -= gravity * delta	#gravity if slope is too steep
	else:
		if !$GroundRay.is_colliding():
			has_contact = false
		velocity.y -= gravity * delta	#gravity if not on  ground
		
	if has_contact and !is_on_floor():
# warning-ignore:return_value_discarded
		move_and_collide(Vector3(0, -1, 0))
	
	if direction.length() > 0 and $StepRay.is_colliding():
		var step_normal = $StepRay.get_collision_normal()
		var step_angle = rad2deg(acos(step_normal.dot(Vector3(0, 1, 0))))
		if step_angle < MAX_STEP_ANGLE:
			var step_height = $StepRay.get_collision_point().y - translation.y
			velocity.y = STEP_JUMP_VEL * step_height
			#has_contact = false
	
	if has_contact and Input.is_action_just_pressed("move_jump"):
		velocity.y += JUMP_VEL
		has_contact = false
	
	#apply velocity
	var temp_velocity = velocity
	temp_velocity.y = 0
	var acceleration
	if direction.dot(temp_velocity) > 0:
		acceleration = WALK_ACCEL
	else:
		acceleration = WALK_DECEL
	
	#apply velocity
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	velocity.x = temp_velocity.x
	velocity.z = temp_velocity.z
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	direction.y = 0
	direction = direction.normalized()
	$StepRay.translation.x = -direction.x
	$StepRay.translation.z = -direction.z


func move_noclip(delta):
	#reset player dir
	direction = Vector3()
	
	#get cam rotation
	var aim = $Head/Camera.get_global_transform().basis
	
	#inputs
	if Input.is_action_pressed("move_forward"):
		direction -= aim.z
	if Input.is_action_pressed("move_backward"):
		direction += aim.z
	if Input.is_action_pressed("move_left"):
		direction -= aim.x
	if Input.is_action_pressed("move_right"):
		direction += aim.x
	if Input.is_action_pressed("move_down"):
		direction -= Vector3(0,1,0)
	if Input.is_action_pressed("move_up"):
		direction += Vector3(0,1,0)
	direction = direction.normalized()
	
	var target = direction * FLY_SPEED
	if Input.is_action_pressed("move_slow"):
		target = direction * FLY_SPEED_CROUCH
	elif Input.is_action_pressed("move_sprint"):
		target = direction * FLY_SPEED_SPRINT
	
	var temp_velocity = velocity
	temp_velocity.y = 0

	var acceleration
	if direction.dot(temp_velocity) > 0:
		acceleration = FLY_ACCEL
	else:
		acceleration = FLY_DECEL
	
	#apply velocity
	velocity = velocity.linear_interpolate(target, acceleration * delta)
	velocity = move_and_slide(velocity)


func _input(event):
	if event is InputEventMouseMotion:
		camera_change = event.relative


func aim():
	if camera_change.length() > 0:
		$Head.rotate_y(deg2rad(-camera_change.x * mouse_sensitivity))
		var change = camera_change.y * mouse_sensitivity
		if camera_angle + change < 90 and camera_angle + change > -90:
			$Head/Camera.rotate_x(deg2rad(-change))
			camera_angle += change
		camera_change = Vector2 (0,0)
