extends Spatial

var player_root
var Cam
var LookRay
var grabber
var interact_message

const MAX_INTERACT_DISTANCE = 1.5


func _ready():
	if !get_tree().get_network_peer() or is_network_master():
		player_root = get_parent().get_parent().get_parent()
		Cam = get_parent()
		LookRay = Cam.get_node("LookRay")
		grabber = get_parent().get_node("Grabber")
		interact_message = get_parent().get_node("HUD/interact_message")


func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		var text = ""
		
		# Note: These ifs are nested on purpose; carrying an object prevents interaction tree from running.
		if grabber.grabbed_object:
			# Drop object
			if Input.is_action_just_pressed("Interact"):
				grabber.action(null)
		
		# Interaction if tree
		elif LookRay.is_colliding() and LookRay.global_transform.xform(Vector3(0,0,0)).distance_to(LookRay.get_collision_point()) < MAX_INTERACT_DISTANCE:
			
			var target = LookRay.get_collider()
			if target:
				var script = target.get_script()
				
				if !script:
					target = target.get_parent()
					script = target.get_script()
				
				
				if script:
					# Enter vehicle
					if script.get_path() == "res://vehicles/scripts/vehicle_seat.gd":
						if target.is_driver:
							text = "enter " + target.name + "\n(driver)"
						else:
							text = "enter " + target.name
						if Input.is_action_just_pressed("Interact") and !target.player_node:
							if get_tree().get_network_peer():
								target.rpc("_enter_vehicle", player_root.get_path())
							else:
								target.enter_vehicle(player_root.get_path())
							text = ""
					# Grab object
					elif script.get_path() == "res://props/scripts/physics_prop.gd":
						text = "grab object"
						if Input.is_action_just_pressed("Interact"):
							grabber.action(target)
							text = ""
		
		interact_message.text = text
