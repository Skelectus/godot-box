extends Spatial

var res = "res://props/barrel.tscn"

var _timer = null


func _ready():
	_timer = Timer.new()
	add_child(_timer)
	_timer.connect("timeout", self, "_timeout")
	_timer.set_wait_time(rand_range(0.3,2))
	_timer.set_one_shot(false)
	_timer.start()


func _timeout():
	var prop = load(res).instance()
	var dir = -get_global_transform().basis.z
	prop.transform = global_transform
	get_parent().add_child(prop)
	prop.apply_central_impulse(dir * rand_range(200,400))
	
	var tween = Tween.new()
	prop.add_child(tween)
	
	tween.interpolate_callback(prop, 10, "queue_free")
	tween.start()
