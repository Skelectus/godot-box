# Network.gd
# This keeps track of connections and other players.
# Edited to be as easy as possible to understand
extends Node

const DEFAULT_MAX_PLAYERS = 5
const DEFAULT_IP = '127.0.0.1'
const DEFAULT_PORT = 42069
const DEFAULT_PLAYERNAME = "Dave"

var ip
var port
var playername
var max_players # Used by server only

var my_id = 0
var players = { }


func _ready():
	ip = DEFAULT_IP
	port = DEFAULT_PORT
	playername = DEFAULT_PLAYERNAME
	max_players = DEFAULT_MAX_PLAYERS
	
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_disconnected', self, '_on_player_disconnected')
# warning-ignore:return_value_discarded
	get_tree().connect('server_disconnected', self, '_on_server_disconnected')
# warning-ignore:return_value_discarded
	get_tree().connect('network_peer_connected', self, '_on_player_connected')


# Server: This is run when creating a server/host game (obviously)
func create_server():
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, max_players)
	get_tree().set_network_peer(peer)
	my_id = 1
	players.clear()
	players[1] = "host"

# Client: This is run when creating a client game (also obviously)
func join_server():
# warning-ignore:return_value_discarded
	get_tree().connect('connected_to_server', self, '_connected_to_server')
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, port)
	get_tree().set_network_peer(peer)
	players.clear()

# Client: This is called on my game after I connect to a server.
func _connected_to_server():
	my_id = get_tree().get_network_unique_id()
	rpc_id(1, "_request_map", my_id)

# Anyone: This is called once by each peer I connect to, as soon as I or they join the server.
func _on_player_connected(player_id):
	if my_id == 1:
		players[player_id] = "player"
		rpc("_instance_player", player_id)

# Anyone: If anyone else disconnects, their player is removed from my game
func _on_player_disconnected(player_id):
	$"/root/Main/".get_node(str(player_id)).queue_free()
	players[player_id] = null

# Client: If the server disconnects, you'll be sent back to main menu
func _on_server_disconnected():
	$"/root/Main/"._quit_to_main_menu()
	get_tree().set_network_peer(null)
	players.clear()

# Instance player
sync func _instance_player(player_id):
	instance_player(player_id)
	if my_id == 1:
		players[player_id] = "player"

func instance_player(player_id):
	if player_id != my_id:
		var player = load('res://player/player_dummy.tscn').instance()
		player.name = str(player_id)
		player.set_network_master(player_id)
		$"/root/Main/".add_child(player)

# Instances a scene for everyone
sync func _instance_scene(_filename, _origin):
	var scene = load(_filename).instance()
	scene.transform.origin = _origin
	scene.set_network_master(1)
	$'/root/Main/'.add_child(scene)
	return scene

remote func _request_map(player_id):
	var map = $'/root/Main/'.get_filename()
	rpc_id(player_id, "_change_map", map)

remote func _request_players(id):
	if my_id == 1:
		rpc_id(id, "_send_players", players)

remote func _send_players(_players):
	players = _players
	for i in range(players.size()):
		var id = players.keys()[i]
		instance_player(id)

remote func _change_map(map):
# warning-ignore:return_value_discarded
	get_tree().change_scene(map)
