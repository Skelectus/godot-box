extends Spatial

const TITLE = "Remover"
const DESCRIPTION = "Click to remove objects" 

func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		var Cam = get_parent().get_parent()
		var LookRay = Cam.get_node("LookRay")
		
		# Remove prop
		if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
			var target = LookRay.get_collider()
			if target is RigidBody:
				if !get_tree().get_network_peer():
					target.queue_free()
				else:
					$"/root/Main".rpc("delete_object", target.get_path())
