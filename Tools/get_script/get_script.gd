extends Spatial

func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		var Cam = get_parent().get_parent()
		var LookRay = Cam.get_node("LookRay")
		
		if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
			var script = LookRay.get_collider().get_script()
			if script:
				print(script.get_path())
		
		elif Input.is_action_just_pressed("fire1") and LookRay.is_colliding():
			print(LookRay.get_collider().name)
