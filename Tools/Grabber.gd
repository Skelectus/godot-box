extends Spatial

var Cam
var LookRay
var Arm
var grabbed_object	# This is always null if nothing is grabbed

const MAX_GRAB_DISTANCE = 1
const GRAB_ACCEL = 400
const GRAB_DECEL = 600
const THROW_FORCE = 200


func _ready():
	if !get_tree().get_network_peer() or is_network_master():
		Cam = get_parent()
		LookRay = Cam.get_node("LookRay")
		Arm = Cam.get_node("Arm")


func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		# Throw
		if grabbed_object and Input.is_action_just_pressed("fire0"):
			var dir = -Cam.get_global_transform().basis.z
			grabbed_object.transform = global_transform
			grabbed_object.apply_central_impulse(dir * THROW_FORCE)
			grabbed_object = null
			Cam.add_child(Arm)


func _physics_process(_delta):
	if grabbed_object:
		grabbed_object.transform.origin = global_transform.origin


func action(target):
	# Grab object
	if !grabbed_object:
		grabbed_object = target
		Cam.remove_child(Arm)
	# Drop object
	else:
		grabbed_object = null
		Cam.add_child(Arm)
