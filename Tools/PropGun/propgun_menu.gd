extends Control


var paths = ["res://props/", "res://vehicles/"]
var scenes = []


func _ready():
	
	for path in paths:
		var dir = Directory.new()
		dir.open(path)
		dir.list_dir_begin()
		while true:
			var file = dir.get_next()
			if file == "":
				break
			elif file.ends_with(".tscn"):
				scenes.append(path + file)
		dir.list_dir_end()
	
	# Generate menu list item for each item in scenes[]
	for i in range(scenes.size()):
		var new_list_item = Button.new()
		new_list_item.text = scenes[i].get_basename() # removes .tscn suffix
		new_list_item.set_text_align(0)
		new_list_item.set_button_icon(load("res://textures/default.png"))
		
		$Panel/VBoxContainer.add_child(new_list_item)
		new_list_item.connect("pressed", get_parent(), "set_res", [scenes[i]])
