extends Spatial
var res = "res://props/barrel.tscn"

var Player
var Cam
var LookRay
var active = true

const DESCRIPTION = "Left click to spawn an object. Right click to shoot.\nR to select object" 

const SHOOT_FORCE = 200

func _ready():
	Player = get_parent().get_parent().get_parent().get_parent()
	$propgun_menu.hide()


func _process(_delta):
	Cam = get_parent().get_parent()
	if !get_tree().get_network_peer() or is_network_master():
		LookRay = Cam.get_node("LookRay")
	
	# Single player
	if !get_tree().get_network_peer():
		if active:
			# Spawn prop
			if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
				var prop = $"/root/Main/".instance_object(res)
				prop.transform.origin = LookRay.get_collision_point()
			# Shoot prop
			elif Input.is_action_just_pressed("fire1"):
				var prop = $"/root/Main/".instance_object(res)
				var dir = -Cam.get_global_transform().basis.z
				prop.transform = global_transform
				prop.apply_central_impulse(dir * SHOOT_FORCE)
	
	
	# Multiplayer
	elif is_network_master():
		# Spawn prop
		if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
			var point = LookRay.get_collision_point()
			
			if get_tree().get_network_unique_id() == 1:
				server_spawn(res, point)
			else:
				rpc_id(1, "_server_spawn", res, point)
		# Shoot prop
		elif Input.is_action_just_pressed("fire1"):
			var _transform = global_transform
			var dir = -Cam.get_global_transform().basis.z
			var _force = dir * SHOOT_FORCE
			
			if get_tree().get_network_unique_id() == 1:
				server_shoot(res, _transform, _force)
			else:
				rpc_id(1, "_server_shoot", res, _transform, _force)
	
	if !get_tree().get_network_peer() or is_network_master():
		# Toggle tool menu
		if Input.is_action_pressed("tool_reload"):
			$propgun_menu.show()
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			Player.override = true
			active = false
		else:
			$propgun_menu.hide()
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			Player.override = false
			active = true


# Spawn prop
# Call this if you're in a client game
remote func _server_spawn(_res, _point):
	if get_tree().get_network_unique_id() == 1:
		server_spawn(_res, _point)

# Call this if you're in the server game
func server_spawn(_res, _point):
	var prop = $"/root/Main/".instance_object(_res)
	prop.transform.origin = _point
	rpc("client_spawn", _res, _point, prop.name)

# Don't call this.
remote func client_spawn(_res, _point, _name):
	var prop = $"/root/Main/".instance_existing_object(_res, _name)
	prop.transform.origin = _point


# Shoot prop
# Call this if you're in a client game
remote func _server_shoot(_res, _transform, _force):
	if get_tree().get_network_unique_id() == 1:
		server_shoot(_res, _transform, _force)

# Call this if you're in the server game
func server_shoot(_res, _transform, _force):
	var prop = $"/root/Main/".instance_object(_res)
	prop.transform = _transform
	prop.apply_central_impulse(_force)
	rpc("client_shoot", _res, _transform, _force, prop.name)

# Don't call this.
remote func client_shoot(_res, _transform, _force, _name):
	var prop = $"/root/Main/".instance_existing_object(_res, _name)
	prop.transform = _transform
	prop.apply_central_impulse(_force)

func set_res(_res):
	print(_res)
	res = _res
