extends Spatial

var HUD
var tool_name_label
var tool_desc_label

var tools
var Selected = 0


func _ready():
	if !get_tree().get_network_peer() or is_network_master():
		HUD = get_parent().get_node("HUD")
		tool_name_label = HUD.get_node("tool_name_label")
		tool_desc_label = HUD.get_node("tool_desc_label")
		
		tools = self.get_children()	
		
		# Unparent all tools
		for i in range(tools.size()):
			remove_child(tools[i])
		
		# Equip first tool
		change_tool(0)



func _process(_delta):
	if !get_tree().get_network_peer() or is_network_master():
		if Input.is_action_just_pressed("tool_next"):
			change_tool(1)
		elif Input.is_action_just_pressed("tool_prev"):
			change_tool(-1)


func _input(event):
	if !get_tree().get_network_peer() or is_network_master():
		if event is InputEventMouseButton:
			if event.is_pressed():
				if event.button_index == BUTTON_WHEEL_UP:
					change_tool(Selected+1)
				elif event.button_index == BUTTON_WHEEL_DOWN:
					change_tool(Selected-1)


func change_tool(num):	
	# Unparent previously selected tool
	if tools[Selected].get_parent() == self:
		remove_child(tools[Selected])
	
	# Set new Selected number
	if num >= tools.size():
		num = 0
	if num < 0:
		num = tools.size() - 1
	Selected = num
	
	# Parent the newly selected tool
	add_child(tools[Selected])
	
	# Put tool title on HUD
	if tools[Selected].get("TITLE"):
		tool_name_label.text = tools[Selected].TITLE
	else:
		tool_name_label.text = tools[Selected].name
	
	# Put tool description on HUD
	if tools[Selected].get("DESCRIPTION"):
		tool_desc_label.text = tools[Selected].DESCRIPTION
	else:
		tool_desc_label.text = "tooldescriptionhere"
