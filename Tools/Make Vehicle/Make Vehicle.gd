extends Spatial

const DESCRIPTION = "Left click to turn a prop into a vehicle. Right click to turn a vehicle into a prop." 

var vehicle_camera = "res://vehicles/default/vehicle_camera.tscn"


func _process(_delta):
	
	# Singleplayer
	if !get_tree().get_network_peer():
		var Cam = get_parent().get_parent()
		var LookRay = Cam.get_node("LookRay")
		
		if LookRay.is_colliding():
			var target = LookRay.get_collider()
			
			# Make vehicle
			if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
				if target.get_script() and target.get_script().get_path() == "res://props/scripts/physics_prop.gd":
					var script = preload("res://vehicles/scripts/vehicle_seat.gd")
					var new_camera = load(vehicle_camera).instance()
					target.add_child(new_camera)
					target.set_script(script)
					target.camera = new_camera.get_node("pivot_x/Camera")
			
			# Unmake vehicle
			elif Input.is_action_just_pressed("fire1") and LookRay.is_colliding():
				if target.get_script() and target.get_script().get_path() == "res://vehicles/vehicle_seat.gd":
					var script = preload("res://props/scripts/physics_prop.gd")
					target.camera.queue_free()
					target.set_script(script)
	
	# Multiplayer
	elif is_network_master():
		var Cam = get_parent().get_parent()
		var LookRay = Cam.get_node("LookRay")
		
		if LookRay.is_colliding():
			var target = LookRay.get_collider()
			
			# Make vehicle
			if Input.is_action_just_pressed("fire0") and target.get_script().get_path() == "res://props/scripts/physics_prop.gd":
				rpc("_make_vehicle", target.get_path())
			
			# Unake vehicle
			elif Input.is_action_just_pressed("fire1") and target.get_script().get_path() == "res://vehicles/vehicle_seat.gd":
				rpc("_unmake_vehicle", target.get_path())


sync func _make_vehicle(path):
	var target = get_node(path)
	var script = preload("res://vehicles/scripts/vehicle_seat.gd")
	var new_camera = load(vehicle_camera).instance()
	target.add_child(new_camera)
	target.set_script(script)
	target.camera = new_camera


sync func _unmake_vehicle(path):
	var target = get_node(path)
	target.rpc("_exit_vehicle")
	var script = preload("res://props/scripts/physics_prop.gd")
	target.camera.queue_free()
	target.set_script(script)
