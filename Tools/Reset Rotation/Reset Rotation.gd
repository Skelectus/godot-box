extends Spatial

var Cam
var LookRay


func _ready():
	if !get_tree().get_network_peer() or is_network_master():
		Cam = get_parent().get_parent()
		LookRay = Cam.get_node("LookRay")


func _process(_delta):
	if !get_tree().get_network_peer():
		Cam = get_parent().get_parent()
		LookRay = Cam.get_node("LookRay")
		
		# Rotate prop
		if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
			var target = LookRay.get_collider()
			if target is RigidBody:
				target.set_rotation_degrees(Vector3(0,0,0))
	
	# Multiplayer call
	elif is_network_master():
		if Input.is_action_just_pressed("fire0") and LookRay.is_colliding():
			rpc('_rotate', LookRay.get_collider().get_path())


sync func _rotate(path):
	var target = get_node(path)
	if target is RigidBody:
		target.set_rotation_degrees(Vector3(0,0,0))
