extends VBoxContainer

var game_create_menu
var button = "res://scenes/mainmenu/map_list_item.tscn"
var path = "res://maps/"
var maps = []


func _ready():
	game_create_menu = $"/root/Main/game_create_menu"
	
	# Add any .tscn files in path to maps[]. For example default.tscn
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(".tscn"):
			maps.append(file)
	dir.list_dir_end()
	
	# Generate button for each item in maps[]
	for i in range(maps.size()):
		var new_list_item = Button.new()
		new_list_item.text = maps[i].get_basename() # get_basename() removes .tscn suffix
		
		# Visual properties
		new_list_item.set_text_align(0)
		new_list_item.set_expand_icon(true)
		new_list_item.set_custom_minimum_size(Vector2(0,64))
		
		# This will load "res://maps/" + "mapname" + ".png"
		var thumbnail = load(path + maps[i].get_basename() + ".png")
		if !thumbnail:
			thumbnail = load("res://textures/default.png")
		new_list_item.set_button_icon(thumbnail)
		
		add_child(new_list_item)
		new_list_item.connect("pressed", game_create_menu, "_change_map", [path + maps[i]])
		
