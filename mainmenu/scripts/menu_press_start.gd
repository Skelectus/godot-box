extends Control

var any_key_pressed = false


# Exit press start screen if any key or mouse button is pressed
func _input(event):
	if event is InputEventKey or event is InputEventMouseButton:
		if event.pressed and is_visible_in_tree():
			any_key_pressed = true


# This is here because we want one frame delay. It prevents the mouse click from triggering buttons in the next menu.
func _process(_delta):
	if any_key_pressed:
		any_key_pressed = false
		
		$"/root/Main/main_menu".show()
		hide()
