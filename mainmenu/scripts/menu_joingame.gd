extends Control


var scene = "res://scenes/testgrounds.tscn"

var name_field
var port_field
var ip_field
var main_menu


func _ready():
	main_menu = $"/root/Main/main_menu"
	
	name_field = $HBoxContainer/start_menu/name_field
	port_field = $HBoxContainer/start_menu/port_field
	ip_field = $HBoxContainer/start_menu/ip_field
	
	name_field.text = Network.DEFAULT_PLAYERNAME
	port_field.text = str(Network.DEFAULT_PORT)
	ip_field.text = str(Network.DEFAULT_IP)


func load_game():
	get_tree().paused = false
	$"/root/Main".queue_free()
# warning-ignore:return_value_discarded
	#get_tree().change_scene(scene)


func _on_join_button_pressed():
	get_tree().paused = true
	Network.join_server()
	load_game()


func _on_name_field_text_changed(new_text):
	Network.name = new_text


func _on_port_field_text_changed(new_text):
	Network.port = new_text.to_int()


func _on_ip_field_text_changed(new_text):
	Network.ip = new_text


func _on_back_button_pressed():
	get_tree().paused = false
	main_menu.show()
	hide()


