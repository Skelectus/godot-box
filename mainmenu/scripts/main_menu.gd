extends CenterContainer

var game_create_menu
var game_join_menu
var press_start_screen


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
	game_create_menu = $"/root/Main/game_create_menu"
	game_join_menu = $"/root/Main/game_join_menu"
	press_start_screen = $"/root/Main/press_start"


func _on_create_menu_button_pressed():
	get_tree().paused = true
	game_create_menu.show()
	hide()


func _on_join_menu_button_pressed():
	get_tree().paused = true
	game_join_menu.show()
	hide()


func _on_quit_game_pressed():
	get_tree().quit()
