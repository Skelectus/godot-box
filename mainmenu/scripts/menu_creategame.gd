extends Control

var enable_multiplayer = false
var map = ""

var name_field
var port_field
var maxp_field
var start_button
var map_label
var main_menu


func _ready():
	main_menu = $"/root/Main/main_menu"
	
	name_field = $HBoxContainer/start_menu/name_field
	port_field = $HBoxContainer/start_menu/port_field
	maxp_field = $HBoxContainer/start_menu/maxp_field
	start_button = $HBoxContainer/start_menu/start_button
	map_label = $HBoxContainer/start_menu/map_label
	
	name_field.text = Network.DEFAULT_PLAYERNAME
	port_field.text = str(Network.DEFAULT_PORT)
	maxp_field.text = str(Network.DEFAULT_MAX_PLAYERS)


func load_game():
	get_tree().paused = false
# warning-ignore:return_value_discarded
	get_tree().change_scene(map)


func _on_start_button_pressed():
	if enable_multiplayer:
		Network.create_server()
	load_game()


func _on_multiplayer_check_toggled(_button_pressed):
	if enable_multiplayer:
		enable_multiplayer = false
		name_field.set_editable(false)
		port_field.set_editable(false)
		maxp_field.set_editable(false)
	
	else:
		enable_multiplayer = true
		name_field.set_editable(true)
		port_field.set_editable(true)
		maxp_field.set_editable(true)


func _on_name_field_text_changed(new_text):
	Network.name = new_text


func _on_port_field_text_changed(new_text):
	Network.port = new_text.to_int()


func _on_maxp_field_text_changed(new_text):
	Network.max_players = new_text.to_int()


func _on_back_button_pressed():
	get_tree().paused = false
	main_menu.show()
	hide()


func _change_map(_map):
	map = _map
	start_button.set_disabled(false)
	map_label.text = _map
