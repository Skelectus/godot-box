extends Control

var path = "res://mainmenu_backgrounds/"
var scenes = []

func _ready():
	get_tree().paused = false
	randomize()
	
	# Add any .tscn files in path to array. For example default.tscn
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(".tscn"):
			scenes.append(file)
	dir.list_dir_end()
	
	# Select a random background scene
	var _filename = scenes[rand_range(0,scenes.size())]
	var scene = load(path + _filename).instance()
	add_child(scene)
