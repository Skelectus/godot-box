extends Spatial


var ray

var flash_reload_time = .5
var tween
var ready = true

var speedlimit = 50

func _ready():
	ray = $RayCast
	tween = Tween.new()
	add_child(tween)


func _process(_delta):
	#if $flash.is_visible(): $flash.hide()
	
	if ready:
		if ray.is_colliding():
			var target = ray.get_collider()
			if target is RigidBody:
				if target.linear_velocity.length() * 3.6 > speedlimit:
					ready = false
					$flash.show()
					# Hide flash after .1 seconds
					tween.interpolate_callback($flash, 0.05, "hide")
					# Set ready to true after flash_reload_time
					tween.interpolate_callback(self, flash_reload_time, "reset")
					tween.start()


func reset():
	ready = true
