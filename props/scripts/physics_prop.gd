extends RigidBody

puppet var _transform

func _ready():
	_transform = transform

func _physics_process(_delta):
	if get_tree().get_network_peer():
		if is_network_master():
			rset_unreliable('_transform', transform)
		else:
			transform = _transform

func _process(_delta):
	if get_tree().get_network_peer():
		
		# If this prop is removed, remove other players' instances of this prop too
		if is_queued_for_deletion():
			print("removed: " + name)
			#rpc('_remove')

remote func _remove():
	queue_free()
